import re

def parse_viavi_csv(filename):
    section = None

    start_frequency = None
    stop_frequency = None
    num_points = 2001

    frequency_step = None

    data_y = []
    data_x = []

    with open(filename, 'r') as data_file:
        for line in data_file:
            if line[0] == "<":
                section = line.strip()[1:-1]
            else:
                if section == "Body":
                    parts = line.strip().split(",")
                    if parts[0] == "Start Frequency (MHz)":
                        start_frequency = float(parts[2])
                        frequency = start_frequency
                    elif parts[0] == "Stop Frequency (MHz)":
                        stop_frequency = float(parts[2])
                    elif parts[0] == "Data Point":
                        num_points = int(parts[2])
                elif section == "Measurement Data":
                    if len(line.strip()) == 0:
                        continue

                    if frequency_step is None:
                        frequency_step = ((stop_frequency - start_frequency) /
                                          (num_points - 1))
                    data = float(line.strip())
                    frequency += frequency_step
                    data_x.append(frequency)
                    data_y.append(data)

    return data_x, data_y
