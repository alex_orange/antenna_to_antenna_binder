import sys
import matplotlib
import matplotlib.pyplot
import parse_viavi

filename = sys.argv[1]

data_x, data_y = parse_viavi.parse_viavi_csv(filename)

fig, ax = matplotlib.pyplot.subplots()
ax.plot(data_x, data_y)
ax.axhline(-65)
fig.show()
